# Movir API Team - Tools

To view this as a (Powerpoint-like) presentation  
AND if you have node/npm installed ...   
clone this repo and execute:

```bash
npm install -g reveal-md
reveal-md -w README.md --theme moon --highlight-theme vs
```

----

## What do we need?

1. A local workstation. 
1. A hosting environment. 
1. Build- & automation Tools; a CI/CD platform. 

----

### General principles

1. Open Source. Vendor indepdenent. 
1. Automate everything. Everything-as-code. 
1. CLI for installation, configuration and deployment. 
1. Infrastructure-as-Code. Immutable infrastructure. 
1. Cross-platform, polyglot-programming.
1. ASP.NET Core as first-choice runtime. 

---

## Local Workstation

Requirements:  
* Blazing fast 
* Local storage to the max 
* Secure and compliant
* Portable

----

Our Local Workstation List:
1. Movir Virtual Desktop
1. NN Managed Laptops
1. BYO (Mac)

Consists of:  
1. Operating System. 
1. Package Manager(s).
1. IDE (Integrated Development Environment).
1. Developer Tools.  


----

## Operating System

Requirements:
* Freedom.
* Full local admin rights.
* Unlimited internet access. 

Our list of Local Workstation operating systems:  
* (Linux?)
* Windows
* Mac

----

## Package Manager(s)

> Use this as much as possible when installing software.

Requirements:
* Large software package support. 

Our package manager list:  
1. Windows: Chocolatey  <https://chocolatey.org/>  
1. Mac: Brew  <https://brew.sh/>     
1. Linux: apk  

----

### IDE(s)

Requirements:
* Support for ASP.NET Core
* Support for other languages and frameworks that come along. 
* Help in writing better code. 
* Command-line and terminal integration. 
* Multi-OS. 

Our list of IDE's - feel free to choose: 
* VSCode  <https://code.visualstudio.com/>  
* JetBrains Rider  <https://www.jetbrains.com/rider>   
* Visual Studio  <https://visualstudio.microsoft.com/>    

---

## Hosting Environment

Consists of: 
1. Runtime Container for your apps. 
1. Database.
1. Event Bus.
1. Hosting Provider.  
 
----

### A runtime container for you apps

Requirements:
* Open Source, vendor independent. 
* Support for a variety of SDK's  
(e.g. ASPNET.Core, Java, Python, Node.js, GoLang, ...). 
* Deployable to the Major Cloud vendors. 
* Runnable on a developers workstation.  
  
Our list of runtimes: 
* Windows ;-( 
* Docker.
* Kubernetes.

----

### Database

Requirements:  
* Mature SDK's for the major programming languages. And thus:   
* Support for the ASPNET.Core SDK. 
* An in-memory implementation of the database for Unit Testing. 

Our list of databases: 
* MS SQL ;-(  
* ...?

----

### Hosting Provider

Requirements:
* Cheap, reliable, secure, compliant. 
* Support for all the technologies that we chose. 

Our hosting list:  
* AWS (Amazon Web Services)
* On premise Movir (Windows) datacenter. 
* ...?


---

## Build- & Automation Tools; CI/CD platform

Requirements:
* Local automation = Pipeline automation. 

Our list of build-, automation- & CI/CD tools: 
* `dotnet build`
* Make
* Docker
* Gitlab
* AWS-CLI, CloudFormation

---
