# Movir API Team - NN Windows Laptop as your Local Workstation

To view this as a (Powerpoint-like) presentation  
AND if you have node/npm installed ...   
clone this repo and execute:

```bash
npm install -g reveal-md
reveal-md -w README.md --theme moon --highlight-theme vs
```

----

## Notes for Mac users

* Make sure your input layout is "US"; and not "US International". Otherwise there's no quote or double-quote without pressing the spacebar each time. 
* Make sure that tabs in your Makefile are stored as tabs instead of spaces. In Jetbrains Rider, via Settings/Editor/Code Style/Other File Types.  Check "Use Tab Character". 

## Notes in general

* The whole idea of working on an NN laptop as a developer, is to work as a `regular` windows user. 
Only when actions need `elevated` (=admin) rights, you enter the credentials of the WSA-account. 
Setting a different finger in Windows Hello helps in not having to enter passwords each time. 

---- 

## Install the Package Manager

* Chocolatey - package manager for Windows.  
<https://chocolatey.org/>  

Install from an Administrative PowerShell with:

```ps
Set-ExecutionPolicy RemoteSigned
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

Create the following directories within that same shell:

```ps
mkdir c:\dev
mkdir c:\dev\downloads
mkdir c:\dev\downloads\choco
```

Change the Chocolatey cache directory, if you like ...

```ps
choco config set cacheLocation c:\dev\downloads\choco
```

----

Install everything that you need, from a powershell script 

```ps
. ./windows/software.ps1
Install
```

Install a couple of packages manually, because they don't work with choco. The powershell script tells you which. 

----

Upgrade everything, from a powershell script 

```ps
. ./windows/software.ps1
Upgrade
```

----

## Restart your workstation?

Unclear is weither you have to reboot; just to be sure ... do it. 

## Connect to Gitlab

Create your SSH keys with help of a powershell script. 

```ps
. ./windows/gitlab.ps1
Gitlab
```

The contents of the generated public key has been copied to the clipboard. 
So: paste your public key in your personal Gitlab settings (tab SSH Keys). 

Test if your SSH connection to Gitlab works. 

```ps
ssh -T git@vcs.aws.insim.biz -v
```

Test if your SSH connection from Git Bash works. 

```gitbash
ssh -T git@vcs.aws.insim.biz -v
```

---