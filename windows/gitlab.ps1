Write-Host (Get-Date).ToString()  "Running script" $PSCommandPath "..."

# Imported scripts
. ./utils.ps1

# Variables
$gitlabSshPassphrase = "dummypassphrase"
$gitlabHostname = "vcs.aws.isim.biz"
$gitlabUsername = "$env:username"
$gitlabUserAtHost = "$gitlabUsername@$gitlabHostname"
$gitlabKeyname = "id_$gitlabUserAtHost"
$gitlabKeyFilepath = "$HOME/.ssh/id_ed25519"
$gitlabKeyFilepathPub = "$gitlabKeyFilepath.pub"
$gitlabSshConfigFilepath = "$HOME/.ssh/config"
$gitlabSshKnownHostsFilepath = "$HOME/.ssh/known_hosts"

function Ssh()
{
    # Guus 2018-11-29: using a custom keyfile name did not work.

    _check

    Remove-Item "$gitlabKeyFilepath" -ErrorAction Ignore
    Remove-Item "$gitlabKeyFilepathPub" -ErrorAction Ignore
    $commandOutput = ssh-keygen -t ed25519 -C $gitlabUserAtHost -f  $gitlabKeyFilepath -N $gitlabSshPassphrase
    $commandOutput      # echoes the command output as a list

    if ($commandOutput -like "*The key fingerprint is*")
    {
        Write-Host " "
        Write-Host "Done! Your private key has been generated in:"
        $gitlabKeyFilepath
        Write-Host "Your public key has been generated in:"
        $gitlabKeyFilepathPub

        # Giving the user the chance to enter an empty passphrase
        ssh-keygen -p -P "$gitlabSshPassphrase" -f "$gitlabKeyFilepath"

        # copy the public key to the clipboard
        cat $gitlabKeyFilepathPub | clip
        Write-Host "The contents of your public key has been copied to your clipboard, so that you can paste in in Gitlab (personal Settings - SSH Keys)."
    }
    else
    {
        Write-Host " "
        Write-Host "Oos! Something went wrong."
        return
    }

    # Make the generated keyfile know to the SSH agent
    Remove-Item "$gitlabSshKnownHostsFilepath" -ErrorAction Ignore
    Remove-Item "$gitlabSshConfigFilepath" -ErrorAction Ignore
    Set-Content -Value "Host $gitlabHostname" -Path $gitlabSshConfigFilepath
    Add-Content -Value "`tHostname $gitlabHostname" -Path $gitlabSshConfigFilepath
    # IdentityFile = pointing to another keyfile >> won't work on Windows
    #    Add-Content -Value "`tIdentityFile $gitlabKeyFilepath" -Path $gitlabSshConfigFilepath

    # Telling Git (and other Bash-like tools) to take the Windows-Home directory as its $HOME
    $env:HOME = $env:userprofile
    Write-Host -ForegroundColor Green "Beware! Your %HOME% environment variable has been set to your Windows %USERPROFILE% directory. So that Git (and other Bash-like tools) have their homedir set to your Windows home-location."
}

function _check() {
    if (IsAdministrator)
    {
        throw "SSH Keys must be generated as a regular user, not an administrator. Please call this function again from a non-elevated shell."
    } else {
        # All OK
    }
}