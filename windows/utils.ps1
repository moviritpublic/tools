Write-Host (Get-Date).ToString()  "Running script" $PSCommandPath "..."

function IsAdministrator()
{
    $user = [Security.Principal.WindowsIdentity]::GetCurrent();
    (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}